var elem1 = document.getElementById('C1'); //Busca la etiqueta con ese id
console.log(elem1.innerHTML); //Devuelve el texto dentro de mi etiqueta

// /n sirve para dar saltos de linea

function info() {
    alert('Elemento Seleccionado: ' + elem1.innerHTML +
        '\n ID elemento: ' + elem1.getAttribute('id') +
        '\n ISO ID:  ' + elem1.getAttribute('data-id') +
        '\n Dial Code ' + elem1.getAttribute('data-dial-code'));
}
var elem2 = document.getElementById('C2');
console.log(elem2.innerHTML);

function info2() {
    alert('Elemento Seleccionado: ' + elem2.innerHTML +
        '\n ID elemento: ' + elem2.getAttribute('id') +
        '\n ISO ID:  ' + elem2.getAttribute('data-id') +
        '\n Dial Code ' + elem2.getAttribute('data-dial-code'));
}
var elem3 = document.getElementById('C3');
console.log(elem3.innerHTML);

function info3() {
    alert('Elemento Seleccionado: ' + elem3.innerHTML +
        '\n ID elemento: ' + elem3.getAttribute('id') +
        '\n ISO ID:  ' + elem3.getAttribute('data-id') +
        '\n Dial Code ' + elem3.getAttribute('data-dial-code'));
}