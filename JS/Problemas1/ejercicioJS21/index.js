function foo() {
    const a = true;

    function bar() {
        const a = false; //Variable diferente
        console.log(a); //false
    }
    const a = true; //desde aqui ya no funcionara
    a = false;
    console.log(a); //true
}
//Asì no ejecuta ya que la variable a ya no puede volver a declararse.

//Cuando se declara una una variable con la palabra clave const implica que no se le permite reasignar el valor, pero declararlo en una función creará un nuevo alcance y con eso una nueva variable.