for (var i = 0; i < 5; i++) {
    nextLoop2Iteration: for (var j = 0; j < 5; j++) {
        if (i == j) break nextLoop2Iteration;
        console.log(i, j);
    }
}
/* 
1 0
2 0
2 1
3 0
3 1
3 2
4 0
4 1
4 2
4 3
*/

//Utiliza etiquetas de roptura y continuación, su definición es la siguiente:
//Las declaraciones de ruptura y continuación pueden ir seguidas de una etiqueta opcional que funciona como una especie de instrucción goto, que reanuda la ejecución desde la posición de la etiqueta referenciada.