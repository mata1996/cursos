var a = 'hello' || '';
console.log(a);
//Devuelve hello ya que en la variable se indica que contenga el 1er valor "o" el segundo que es un espacio vacio, ya que se utilizaa el operador Or, este indica que tome el primer valor o el 2do pero siempre toma el 1ro y en caso de no contener nada ya pasaria al 2do.
var b = '' || [];
console.log(b);
//Devuelve [] ya que en la variable se indica que contenga el 1er valor "o" el segundo, ya que se utilizaa el operador Or, este indica que tome el primer valor o el 2do pero siempre toma el 1ro pero como esta vacio entonces agarra el segundo.
var c = 'hello' || undefined;
console.log(c);
//Devuelve hello ya que en la variable se indica que contenga el 1er valor "o" el segundo, ya que se utilizaa el operador Or, este indica que tome el primer valor o el 2do pero siempre toma el 1ro y en caso de no contener nada ya pasaria al 2do.
var d = 1 || 5;
console.log(d);
//Devuelve 1 ya que en la variable se indica que contenga el 1er valor "o" el segundo, ya que se utilizaa el operador Or, este indica que tome el primer valor o el 2do pero siempre toma el 1ro y en caso de no contener nada ya pasaria al 2do.
var e = 0 || {};
console.log(e);
//Devuelve {} ya que en la variable se indica que contenga el 1er valor "o" el segundo, ya que se utilizaa el operador Or, este indica que tome el primer valor o el 2do pero siempre toma el 1ro y en caso de no contener nada ya pasaria al 2do, en este caso  le da prioridad al 2do ya que el 1ro es un 0.
var f = 0 || '' || 5;
console.log(f);
//Devuelve 5 ya que en la variable se indica que contenga el 1er valor "o" el segundo "o" tercer, ya que se utilizaa el operador Or, este indica que tome el primer valor o el 2do o el 3ro pero siempre toma el 1ro y en caso de no contener nada ya pasaria al 2do, en este caso  le da prioridad al 3ro ya que el 1ro es un 0, el 2do un espacio vacio y en el tercero contiene el numero mayor.
var g = '' || 'yay' || 'boo';
console.log(g);
//Devuelve yay ya que en la variable se indica que contenga el 1er valor "o" el segundo "o" tercer, ya que se utilizaa el operador Or, este indica que tome el primer valor o el 2do o el 3ro pero siempre toma el 1ro y en caso de no contener nada ya pasaria al 2do, en este caso  le da prioridad al 2do ya que el 1ro es un espacio vacio, el 2do un el primer valor y en el tercero igual un valor pero el 2do esta primero