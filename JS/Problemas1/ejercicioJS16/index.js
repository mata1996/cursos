/*var isLegal = age >= 18;
var tall = height >= 5.11;
var suitable = isLegal && tall;
var isRoyalty = status === 'royalty';
var specialCase = isRoyalty && hasInvitation;
var canEnterourBar = suitable || specialCase;
//Nos va a marcar error por que:
console.log(isLegal); //no se define age
console.log(tall); //no se define height
console.log(suitable); //falta las variables definidas
console.log(isRoyalty); //no se define status
console.log(specialCase); //no se define hasInvitation
console.log(canEnterourBar); //falta las variables definidas*/


//El còdigo deberia quedar así:

var age = 22;
var height = 1.70;
var status = 'developer';
var hasInvitation = true;

var isLegal = age >= 18;
var tall = height >= 5.11;
var suitable = isLegal && tall;
var isRoyalty = status === 'royalty';
var specialCase = isRoyalty && hasInvitation;
var canEnterourBar = suitable || specialCase;

console.log(isLegal); //nos devuelve true
console.log(tall); //nos devuelve false
console.log(suitable); //nos devuelve false
console.log(isRoyalty); //nos devuelve false
console.log(specialCase); //nos devuelve false
console.log(canEnterourBar); //nos devuelve false


//Estos valores que nos devuelde dependen de las nuevas variables que se declaran y true es cuando se cumple y false cuando no.