var namedSum = function sum(a, b) {
    return a + b;
}
var anonSum = function sum(a, b) {
    return a + b;
}
namedSum(1, 3);
anonSum(1, 3);

console.log(namedSum);
console.log(anonSum);
/*
4
4
*/

//Los nombres son privados