var name = 'John';

function john() {
    return 'John';
}

function jacob() {
    return 'Jacob';
}

switch (name) {
    case john(): // si name es igual a John se imprime
        console.log('I will run if name === "John"');
        break;
    case 'Ja' + 'ne':
        console.log('I will run if name === "Jane"');
        break;
    case john() + ' ' + jacob() + ' Jingleheimer Schmidt':
        console.log('His name is equal to name too!');
        break; //Sino pasa esto
}

//hay que definir name por que no lo estaba, se le asigna un valor para poder llevar al cabo el switch