var value = 1;
switch (value) {
    case 1:
        console.log('I will always run');
        break;
    case 2:
        console.log('I will never run');
        break;
}
//En caso de que el primer caso falle pasara al segundo
//Todo depende  del valor dela variable value