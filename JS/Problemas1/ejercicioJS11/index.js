var x = "c"
switch (x) {
    case "a":
    case "b":
    case "c":
        console.log("Either a, b, or c was selected.");
        break;
    case "d":
        console.log("Only d was selected.");
        break;
    default:
        console.log("No case was matched.");
        break;
}
//Se podria decir que esta es una forma de omitir el return o breakde los  casos sin afetar la funcionalidad  del swith