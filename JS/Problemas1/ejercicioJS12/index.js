/*var x = 5 + 7;
var x = 5 + "7";
var x = "5" + 7;
var x = 5 - 7;
var x = 5 - "7";
var x = "5" - 7;
var x = 5 - "x";

console.log(x)*/

//Devielve NaN ya que se manda a traer una variable definida y repetida muchas veces
//Lo más optimo seria hace lo siguiente:


var x1 = 5 + 7;
console.log(x1)
var x2 = 5 + "7";
console.log(x2)
var x3 = "5" + 7;
console.log(x3)
var x4 = 5 - 7;
console.log(x4)
var x5 = 5 - "7";
console.log(x5)
var x6 = "5" - 7;
console.log(x6)
var x7 = 5 - "x";
console.log(x7) //Aqui nos retorna un NaN ya que a un numero no se le puede restar un caracter