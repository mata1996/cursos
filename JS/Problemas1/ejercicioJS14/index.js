var a = 'hello' && '';
console.log(a);
//No devuelve nada
var b = '' && [];
console.log(b);
//No devuelve nada
var c = undefined && 0;
console.log(c);
//Devuelve undefined
var d = 1 && 5;
console.log(d);
//Devuelve 5
var e = 0 && {};
console.log(e);
//Devuelve 0
var f = 'hi' && [] && 'done';
console.log(f);
//Devuelve done
var g = 'bye' && undefined && 'adios';
console.log(g);
//Devuelve undefind


//Este operador logico es el de and (y)