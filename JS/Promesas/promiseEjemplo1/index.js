let promise1 = new Promise((resolve, reject) => {
    //Una tarea asincrona usando setTimeou
    //SetTimeOut es un temporalizador en milisegundos
    setTimeout(function() {
        resolve("Resolucion correcta!!");
    }, 2500);
});

promise1.then(mensaje => {
    console.log(`Mensaje de resolucion: ${mensaje}`);
});