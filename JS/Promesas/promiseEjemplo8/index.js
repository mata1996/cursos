//Carga de objetos
function getReq(url) {
    const req = new Promise(function(resolve, reject) {
        const request = new XMLHttpRequest();
        request.addEventListener('load', () => {
            console.log('En el listener de load... ');
            if (request.status === 200) {
                resolve(request.response);
            } else {
                reject(request.statusText)
            }
        });
        request.addEventListener('error', () => {
            console.log('En el listener de error... ');
            request("Error de red");
        });
        request.open('GET', url, true);
        request.send(null);
    });
    return req;
}
getReq('persona.json')
    .then(res => {
        return JSON.parse(res);
        //Combierte un JSON a Objeto
    })
    .then(json => {
        console.log(json);
    })
    .catch(error => {
        console.log(error);
    });