obtener_pagina(url)
    .then(
        //Se ejecuta el metodo 'resolve'
        html => { remover_etiquetas(html) }
    )
    .then(
        texto => { guardar_texto_en_bd(texto) }
    )
    .then(() => {
        //final de la promesa
    })
    .catch(error => {
        //Si ocurre un error y/o se ejecuta el metodo 'reject'
    })
    .finally(() => {
        //se ejecuta si o si al final de todo sin importar si es error o exito
    })