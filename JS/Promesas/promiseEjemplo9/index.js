var personaje = {
    nombre: "Heroe",
    vida: 100,
    fuerza: 10,

    //Retoma la descripcion del personaje
    describir: function() {
        var descripcion =
            this.nombre +
            " tiene " +
            this.vida +
            " puntos de vida y " +
            this.fuerza +
            " de fuerza.";
        return descripcion;
    },
};