let promise2 = new Promise(function(resolve, reject) {
        //Una tarea asincrona usando setTimeou
        setTimeout(function() {
            reject('Done!!');
            //reject(Error('Data could not be found'));
        }, 2500);
    })
    .then(function(e) { console.log('done', e); })
    .catch(function(e) { console.log('catch: ', e); });

console.log(promise2);
//From the console: 'catch:Done!!'