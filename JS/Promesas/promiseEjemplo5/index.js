//Encadenamiento
new Promise(function(resolve, reject) {
        //Una tarea ansicrona usando setTimeout
        setTimeout(function() {
            resolve(10);
        }, 250);
    })
    .then(function(num) {
        console.log('first then: ', num);
        return num * 2;
    })
    .then(function(num) {
        console.log('second then: ', num);
        return num * 2;
    })
    .then(function(num) {
        console.log('last then: ', num);
    })