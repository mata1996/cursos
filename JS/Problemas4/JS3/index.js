//Crear una función para imprimir en consola la siguiente seria de números: 0,2,4,...98


//creo una funcion con 2 parametros
//Coloco un ciclo for para que se haga la operacion de multiplicar e imprimo el resultado

function serie(inicio, hasta) {
    for (i = 0; i <= hasta; i++) console.log(inicio * i);
}
//mando a traer mi funcion y defino los valores de los parametros para que en el for puedan utilizarse
serie(2, 49);