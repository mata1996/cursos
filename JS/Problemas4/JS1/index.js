var anObject = {
    foo: 'bar',
    length: 'interesting',
    '0': 'zero!',
    '1': 'one!'
}

var anArray = ['zero.', 'one.'];

//Cual es el resultado del siguiente código y porque ?

console.log(anArray[0], anObject[0]); //El resultado es: zero. zero!
//Este resultado se da debido a que se manda a imprimir  los valores que se encuentran en la posicion 0 del arreglo y del objeto.
console.log(anArray[1], anObject[1]); //El resultado es: one. one!
//Este resultado se da debido a que se manda a imprimir  los valores que se encuentran en la posicion 1 del arreglo y del objeto.
console.log(anArray.length, anObject.length); //El resultado es: 2 'interesting'
//Este resultado se da debido a que se manda a imprimir  en el arreglo con .length el nùmero de elementos que hay dentro del arreglo, como solo hay 2 entonces nos devuelve un 2, y en el objeto nos manda el valor de la atributo que tiene como nombre length que es 'interesting'
console.log(anArray.foo, anObject.foo); //El resultado es: undefined 'bar'
//Este resultado se da debido a que se manda a imprimir  los valores que se encuentran en con el atributo foo, ya que primero indica que se haga del arreglo y como el arreglo no cuenta con ese atributo y no es su sintaxis por eso nos manda un valor indefinido, y en el arreglo es similar a la instruccion anterior ya que se le indica que dentro de el busque el atributo foo y nos mande su valor.



console.log(typeof anArray == 'object', typeof anObject == 'object'); //El resultado es: true true
//Debido a que typeof devuelve una cadena a una variable por eso se asigno con == , y nos devuelve en ambos true ya que se le asigno lo mismo
console.log(anArray instanceof Object, anObject instanceof Object); //El resultado es: true true
//instanceof devuelve en este caso a ambos como true ya que comprueba si estan compustos por objetos.
console.log(anArray instanceof Array, anObject instanceof Array); //El resultado es: true false
//Aqui similar al anterior solo que en lugar de objetos es arreglos, en el primero nos devuelve un true por que si es un arreglo, el segundo como false ya que un arreglo puede componerse por objetos pero un objeto de arreglos no.
console.log(Array.isArray(anArray), Array.isArray(anObject)); //El resultado es: true false
//El metodo isArray nos muestra si es un arreglo o no, como en la instrucción se coloca el metodo seguido de la variable que definimos al principio, como el primero es un arreglo nos devuelve un true, el segundo debido a que es un objeto por lo tal no es un arregle y por eso nos manda un false.