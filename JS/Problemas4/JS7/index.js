//Dado el siguiente código:
class Queue {
    constructor() {
        const list = [];
        this.enqueue = function(type) {
            list.push(type);
            return type;
        };
        this.dequeue = function() {
            return list.shift();
        };
    }
}
//Cual es el resultado de ejecuta las siguientes sentencias y porque? :
var q = new Queue;
q.enqueue(9);
//Nos da como resultado 9 ya que es el valor que sele asigno
q.enqueue(8);
//Nos da como resultado 8 ya que es el valor que sele asigno
q.enqueue(7);
//Nos da como resultado 7 ya que es el valor que sele asigno

console.log(q.dequeue());
console.log(q.dequeue());
console.log(q.dequeue());
console.log(q);
//Nos da como resultado Queue {enqueue: ƒ, dequeue: ƒ} ya que en la nueva variable se asigna a q como una nueva clase Queue y esta clase contiene equeue y dequeue
console.log(Object.keys(q));
//Nos da como resultado ['enqueue', 'dequeue'] ya que Object.keys nos devuelve un array de las propiedades names de un objeto, en el mismo orden como se obtienen en un loop normal