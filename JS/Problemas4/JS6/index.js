//Dado el siguiente código:
const classInstance = new class {
    get prop() {
        return 5;
    }
};

//Cual es el resultado de ejecuta las siguientes sentencias y porque? :

classInstance.prop = 10;
console.log(classInstance.prop);
//El resultado es 5, debido a que es una propiedad que se declaro desde el inicio y no puede sobreescribirse