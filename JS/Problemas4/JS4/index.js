//Dado el siguiente código:
let zero = 0;

function multiply(x) {
    return x * 2;
}

function add(a = 1 + zero, b = a, c = b + a, d = multiply(c)) {
    console.log((a + b + c), d);
}

//Cual es el resultado de ejecutar las siguientes sentencias?
add(1);
//El resultado es: 4 4
add(3);
//El resultado es: 12 12
add(2, 7);
//El resultado es: 18 18
add(1, 2, 5);
//El resultado es: 8 10
add(1, 2, 5, 10);
//El resultado es: 8 10