//Dada la siguiente clase:
class Person {

    _firstname = '';
    _lastname = '';

    set firstname(value) {
        if (value != '') {
            value = '';
        }
        this._firstname = value;
    }
    get firstname() {
        return this._firstname;
    }

    set lastname(value) {
        if (value != '') {
            value = '';
        }
        this._lastname = value;
    }
    get lastname() {
        return this._lastname;
    }

}

//Define los métodos correspondientes para ejecutar el siguiente código:

let person = new Person('John', 'Doe');
//console.log(person.firstname, person.lastname);
person.firstname = 'Foo';
person.lastname = 'Bar';
console.log(person.firstname, person.lastname);

//Nota: las propiedades firstname y lastname deben no ser accesibles desde el exterior.


//Se utiliza el prefijo de subrayado _ para definir propiedades privadas