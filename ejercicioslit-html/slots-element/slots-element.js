import { SlotsElement } from './src/SlotsElement.js';
import { DynamicStyles } from './src/DynamicStyles.js';
import { MyElement } from './src/MyElement.js';

window.customElements.define('my-element', MyElement);
window.customElements.define('dynamic-styles', DynamicStyles);
window.customElements.define('slots-element', SlotsElement);