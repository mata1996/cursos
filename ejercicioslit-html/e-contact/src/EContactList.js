import { html, LitElement } from 'lit';
import '../e-contact.js';

export class EContactList extends LitElement {
    static get properties() {
        return {
            contactos: {
                type: Array
            }
        };
    }

    constructor() {
        super();
        this.contactos = [{
                nombre: 'Lucho Godinez',
                email: 'user1_some_email@mail.com'
            },
            {
                nombre: 'Hugo Sanchez',
                email: 'user2_some_email@mail.com'
            },
            {
                nombre: 'Jhon Done',
                email: 'user3_some_email@mail.com'
            }
        ];
    }

    render() {
            return html `
        <div>
            ${this.contactos.map(contact => 
                html`<e-contact 
                nombre="${contact.nombre}"
                email="${contact.email}"></e-contact>`)}
        </div>
        `;
    }
}