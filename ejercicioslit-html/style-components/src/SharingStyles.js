import { html, css, LitElement } from "lit";
import { buttonStyle } from "./module-styles.js";

export class SharingStyles extends LitElement {
    static get style() {
        return [
            buttonStyle,
            css `
            :host{
                display: block;
                border: 1px solid black;
            }`
        ]
    }

    render() {
        return html `
        <button class="blue-button">Click</button>
        <button class="blue-button-" disabled>no click</button>
        `;
    }
}