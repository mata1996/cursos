import { css } from 'lit';
import { StyleComponents } from './StyleComponents.js';

export class HerenciaElemento extends StyleComponents {
    static get styles() {
        return [
            super.styles,
            css `button{color: red;}`
        ];
    }
}