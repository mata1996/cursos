import { html, css, LitElement } from 'lit';

export class StyleComponents extends LitElement {
    static get styles() {
        return css `
        *{color: red;}
        p{font-family: sans-serif;}
        .myclass{margin: 100px;}
        #main{padding: 30px;}
        h1{font-size: 4em;}
       button {
      width: 300px;
      font-style: italic;
    }
    `;
    }


    render() {
        return html `
        <p>Lorem ipsum dolor sit, amet consectetur adipiscing elit. Voluptatibus .....</p>
        <p class="myclass">Parrafo 1</p>
        <p id="main">Parrafo 2</p>
        <h1>T i t u l o</h1>
      <button>Click</button>
    `;
    }
}