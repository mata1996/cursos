import { StyleComponents } from './src/StyleComponents.js';
import { HerenciaElemento } from './src/HerenciaElemento.js';
import { SharingStyles } from './src/SharingStyles.js';

window.customElements.define('sharing-styles', SharingStyles);
window.customElements.define('herencia-elemento', HerenciaElemento);
window.customElements.define('style-components', StyleComponents);